\documentclass[a4paper,oneside]{article}
\usepackage{lipsum}
\usepackage{amsfonts}
\usepackage{t1enc}
\usepackage{xfrac}
\usepackage{subcaption}
\usepackage{graphicx}
\usepackage{fancyref}

\frenchspacing

\newcommand{\dd}{\mathrm{d}}

\begin{document}

\section{Problem statement}
Segment blood vessels in retinal fundus images! Detect the vessels as continuous lines on the images.

\section{The model}
Chaudhuri \textit{et al.} have observed in \cite{cikk} the following about blood vessels on grayscale images:
\begin{enumerate}
\item
    Blood vessels can be approximated by piecewise linear segments.

\item
    Blood vessels appear darker relative to the background. These vessels almost never have ideal step edges. The intensity profile can be approximated by $f(x,y)=A(1-k \exp (\frac{-d^2}{2 \sigma^2 }))$, where $d$ is the perpendicular distance between the point $(x,y)$ and the straight line passing through the center of the blood vessel in a direction along its length, $\sigma$ defines the spread of the intensity profile, $A$ is the gray level intensity of the local background, and $k$ is a measure of reflectance of the blood vessel relative to its neighbosigmaod.

\item
    The width of the vessels found to lie 2-10 pixels.
\end{enumerate}

If we consider the detection of arbitary signal $s(t)$ in an additive Gaussian white noise, and a filter with transfer function $H(f)$, then the output signal is given by $s_0(t) = \int H(f)(S(f) + \eta(f))\exp (2\pi ift) \dd f$, where $S(f)$ is the Fourier transform of $s(t)$, and $\eta(f)$ is the noise in the spectrum. $S^*(f)$ maximizes the signal-to-noise ratio. The optimal filter with the impulse response $h(t)$ is commonly known as the matched filter for the signal $s(t)$. Typically, there are $n$ different signals, $s_i(t)$, $i=1,\dots,n$, the received signal is passed through a stack of $n$ matched filters. If the response due to the $j$th filter is the maximum, it is concluded that $s_j(t)$ was trasmitted.

In our case the intensity profile is assumed to be symetrical about the line passing through the center of the vessel, $s(t) = s(-t)$. The optimal filter has the same shape the intesity profile itself, and it is given by $h_{\textrm{opt}}(d) = -\exp(\frac{-d^2}{2\sigma^2})$. Also, instead of a classification problem of $n$ different signals, we only want to decide whether a pixel is part of a blood vessel or not. If the magnitude of the filtered ouput in a given pixel location is higher than a threshold, we consider the pixel to be part of a blodd vessel in the picture.

We extend the the concept into 2-D images. Vessel may be oriented at any angle $0 \le \theta < \pi$. The filter must be rotated into all possible angles, the responses are compared, and for each pixel the maximum response is to be retained.

Assuming the background have constant intensity with zero mean Gaussian white noise, the expected value of the filter ouput should be zero. For this reason, the mean value of $s(x, y)$ is substracted from the kernel.

Instead of a single intensity profile of the cross section of a vessel, we consider matching a number of cross sections along its length. The kernel can be expressed with $K(x, y) = \exp(\frac{-x^2}{2\sigma^2})$, for $y \le \frac{\vert L \vert}{2}$, where $L$ is the length of the segment with fixed orientation. For every direction this kernel must be rotated with $\theta$.

\section{The solution}

Let $p = (x, y)$ to be a discrete point in the kernel and $\theta$ the orientation. It is assumed that kernel center around $(0,0)$. Let $Q$ be the 2-D rotation matrix for $\theta$ and $p'= (u, v) = Qp$. For the implementation $n = 12$ different kernel was selected, $\theta_i = \frac{i\pi}{n}$, $i=0,\dots,n-1$.

The gaussian curve is tructated at $u = \pm 3 \sigma$. $N = \{(u,v) \mid |u| \le 3\sigma, |v| \le \frac{|L|}{2} \}$. The corresponging kernel is $K_i(x,y) = -\exp(\frac{-u^2}{2\sigma^2})$, for $p_i' \in N$. Let $A$ the number of points in $N$ and $m_i = \sum_{p_i' \in N_i} \frac{K_i(x, y)}{A}$. The kernel is given by $K_i(x, y) = K_i(x, y) - m_i$. The kernel coefficients are scaled by $10$ and rounded to the nearest integer.

Threshold selection is a twostep process. At first, Otsu's method \cite{otsu} is used to create an initial threshold based on the matched filter's output. As a second step, if we have a training set available, we can further optimize the value numerically. The error function for the optimalization process is the maximum values of Matthews correlation coefficent, negated.

\section{Results}

\begin{figure}
    \centering
    \includegraphics[height=0.33\paperheight]{test}
    \caption{Original image}\label{fig:test_image}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[height=0.33\paperheight]{otsu_result}
    \caption{Threshold selected with Otsu's method}\label{fig:otsu_image}
\end{figure}

\begin{figure}
    \centering
    \includegraphics[height=0.33\paperheight]{result}
    \caption{Threshold selected with training set}\label{fig:result_image}
\end{figure}

For testing purposes, DRIVE \cite{drive} were used. It contains traingin set of 20 images, and another 20 images for testing. Figure \ref{fig:test_image} shows on of the input images of the test set, Figure \ref{fig:otsu_image} shows the output with the initial threshold selected with Otsu's method, and Figure \ref{fig:result_image} shows the results after tuning the threshold value via the training set. White pixels are the true positive pixels, red pixels are false postive pixels, green ones are the false negative pixels, and the black ones the true negatives. Table \ref{otsu_table} and \ref{test_table} contains vairous statistics about the test set with different thresholds. Overall, we can observe no improvement over Otsu's method, although with different error function we might acieve better results.

\begin{table}
    \caption{Threshold with Otsu's method}
    \label{otsu_table}
    \begin{tabular}{ccccc}
Image & Accuracy & Sensitivity & Precision & Specificity \\
\hline
01 & 89.88 & 25.58 & 90.29 & 99.59 \\
02 & 89.99 & 36.28 & 92.21 & 99.46 \\
03 & 90.89 & 71.57 & 67.74 & 94.19 \\
04 & 90.49 & 40.74 & 77.18 & 98.15 \\
05 & 87.48 & 14.38 & 68.44 & 98.96 \\
06 & 85.51 & 1.90 & 29.41 & 99.25 \\
07 & 88.78 & 24.52 & 72.60 & 98.59 \\
08 & 88.42 & 9.42 & 85.78 & 99.78 \\
09 & 88.54 & 11.99 & 55.66 & 98.73 \\
10 & 82.70 & 85.73 & 39.63 & 82.29 \\
11 & 89.43 & 65.14 & 58.25 & 93.05 \\
12 & 89.97 & 31.30 & 73.27 & 98.37 \\
13 & 87.49 & 19.24 & 72.08 & 98.77 \\
14 & 92.77 & 63.56 & 71.89 & 96.68 \\
15 & 91.32 & 20.83 & 82.40 & 99.48 \\
16 & 89.08 & 26.86 & 72.31 & 98.45 \\
17 & 88.00 & 4.59 & 70.41 & 99.73 \\
18 & 91.26 & 42.45 & 69.63 & 97.60 \\
19 & 89.72 & 20.95 & 76.63 & 99.13 \\
20 & 92.01 & 46.60 & 68.45 & 97.44 \\
    \end{tabular}
\end{table}

\begin{table}
    \caption{Threshold with training set}
    \label{test_table}
    \begin{tabular}{ccccc}
Image & Accuracy & Sensitivity & Precision & Specificity \\
\hline
01 & 89.01 & 17.94 & 90.92 & 99.73 \\
02 & 88.68 & 26.20 & 93.73 & 99.69 \\
03 & 91.95 & 57.81 & 81.52 & 97.77 \\
04 & 89.44 & 28.79 & 78.24 & 98.77 \\
05 & 87.02 & 10.21 & 63.40 & 99.07 \\
06 & 85.47 & 1.04 & 20.58 & 99.34 \\
07 & 88.21 & 18.30 & 71.25 & 98.87 \\
08 & 88.19 & 6.75 & 89.97 & 99.89 \\
09 & 88.20 & 8.00 & 48.61 & 98.88 \\
10 & 91.46 & 68.70 & 63.10 & 94.55 \\
11 & 90.69 & 48.33 & 70.54 & 96.99 \\
12 & 89.11 & 22.22 & 70.62 & 98.68 \\
13 & 86.86 & 13.62 & 68.24 & 98.95 \\
14 & 92.70 & 50.23 & 80.57 & 98.38 \\
15 & 90.85 & 14.15 & 86.20 & 99.74 \\
16 & 88.43 & 20.07 & 70.24 & 98.72 \\
17 & 87.79 & 2.31 & 63.38 & 99.81 \\
18 & 90.36 & 30.10 & 68.16 & 98.17 \\
19 & 89.05 & 14.24 & 73.29 & 99.29 \\
20 & 90.76 & 27.67 & 65.85 & 98.29 \\
    \end{tabular}
\end{table}
\begin{thebibliography}{9}
    \bibitem{cikk}
        Subhasis Chaudhuri, Shankar Chatterjee, Norman Katz, Mark Nelson, Michael Goldbaum.
        \textit{Detection of Blood Vessels in Retinal Images Using Two-Dimensional Matched Filters.}
        IEEE Transactions on Medical Imaging, Vol. 8, No. 3, September 1989a
    \bibitem{otsu}
        Nobuyuki Otsu.
        \textit{A Threshold Selection Method from Gray-Level Histograms.}
        IEEE Transactions on Systems, Man, and Cybernetics ( Volume: 9 , Issue: 1 , Jan. 1979 )
    \bibitem{drive}
        J.J. Staal, M.D. Abramoff, M. Niemeijer, M.A. Viergever, B. van Ginneken.
        \textit{Ridge based vessel segmentation in color images of the retina.}
        IEEE Transactions on Medical Imaging, 2004, nr. 4, vol. 23, pp. 501-509.
\end{thebibliography}
 
\end{document}

