%% constants
L = 9;
sigma = 2;
dirs = 12;

%% rotations
thetas = (0:dirs-1) * pi/dirs;
Rs = zeros(2, 2, dirs);
for i = 1:dirs
    Rs(:,:,i) = [ cos(thetas(i)), -sin(thetas(i)); sin(thetas(i)), cos(thetas(i))];
end;

%% kernel rotated into each directions
% size is selected so that each rotated kernel fit
Ks = zeros(15, 16, dirs);
for i = 1:dirs
    for x = -7:8
        for y = -7:7
            p = [x, y] * Rs(:,:,i)';
            if all(abs(p) <= [3*sigma, L/2])
                Ks(y+8, x+8, i) = -exp(-p(1)^2 / (2*sigma*sigma));
            end;
        end;
    end;
    
    m = sum(sum(Ks(:,:,i))) / sum(sum(Ks(:,:,i) ~= 0));
    Ks(:,:,i) = round(10*(Ks(:,:,i)-m*(Ks(:,:,i) ~= 0)));
end;

%% traning set
training_dir = 'DRIVE/training';
training_set = 21:40;
training_images = cell(length(training_set));
training_masks = cell(length(training_set));
training_results = cell(length(training_set));
training_manuals = cell(length(training_set));

for i = training_set
    image_file = sprintf('%s/images/%d_training.tif', training_dir, i);
    mask_file = sprintf('%s/mask/%d_training_mask.gif', training_dir, i);
    manual_file = sprintf('%s/1st_manual/%d_manual1.gif', training_dir, i);
    idx = i - training_set(1) + 1;
    training_images{idx} = imread(image_file);
    training_masks{idx} = imread(mask_file);
    training_results{idx} = max_conv_by_pixel(training_images{idx}, training_masks{idx}, Ks);
    training_manuals{idx} = imread(manual_file);
end;

%% initial threshold with Otsu's method
init_threshold = graythresh(training_results{1});
init_threshold_error = threshold_error_function(init_threshold, training_results, training_masks, training_manuals);

%% search for threshold with minimum error
threshold = fminsearch(@(t) threshold_error_function(t, training_results, training_masks, training_manuals), init_threshold);
threshold_error = threshold_error_function(threshold, training_results, training_masks, training_manuals);

%% test set
test_dir = 'DRIVE/test';
test_set = 1:20;
test_images = cell(length(test_set));
test_masks = cell(length(test_set));
test_results = cell(length(test_set));
test_result_images = cell(length(test_set));
test_manuals = cell(length(test_set));

for i = test_set
    image_file = sprintf('%s/images/%02d_test.tif', test_dir, i);
    mask_file = sprintf('%s/mask/%02d_test_mask.gif', test_dir, i);
    manual_file = sprintf('%s/1st_manual/%02d_manual1.gif', test_dir, i);
    test_images{i} = imread(image_file);
    test_masks{i} = imread(mask_file);
    test_results{i} = max_conv_by_pixel(test_images{i}, test_masks{i}, Ks);
    test_result_images{i} = imbinarize(test_results{i}, threshold);
    test_manuals{i} = imread(manual_file);
end;

%% otsu stats
otsu_stats = zeros(length(test_set), 4);
for i = test_set
    [true_pos, true_neg, false_pos, false_neg, pos, neg] = image_stats(imbinarize(test_results{i}, init_threshold), test_manuals{i}, test_masks{i});
    otsu_stats(i, 1) = (true_pos + true_neg) / (pos + neg); % accuracy
    otsu_stats(i, 2) = true_pos / (true_pos + false_neg); % sensitivity
    otsu_stats(i, 3) = true_pos / (true_pos + false_pos); % precision
    otsu_stats(i, 4) = true_neg / (true_neg + false_pos); % specificity
    display(sprintf('%02d & %.2f & %.2f & %.2f & %.2f \\\\', i, 100*otsu_stats(i, 1), 100*otsu_stats(i, 2), 100*otsu_stats(i, 3), 100*otsu_stats(i,4)));
end;

%% test result
test_stats = zeros(length(test_set), 4);
for i = test_set
    [true_pos, true_neg, false_pos, false_neg, pos, neg] = image_stats(test_result_images{i}, test_manuals{i}, test_masks{i});
    test_stats(i, 1) = (true_pos + true_neg) / (pos + neg); % accuracy
    test_stats(i, 2) = true_pos / (true_pos + false_neg); % sensitivity
    test_stats(i, 3) = true_pos / (true_pos + false_pos); % precision
    test_stats(i, 4) = true_neg / (true_neg + false_pos); % specificity
    display(sprintf('%02d & %.2f & %.2f & %.2f & %.2f \\\\', i, 100*test_stats(i, 1), 100*test_stats(i, 2), 100*test_stats(i, 3), 100*test_stats(i,4)));
end;

%% show result
figure, imshow(create_result_image(imbinarize(test_results{20}, init_threshold), test_masks{20}, test_manuals{20}));
figure, imshow(create_result_image(test_result_images{20}, test_masks{20}, test_manuals{20}));