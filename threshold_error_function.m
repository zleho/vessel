function [ error ] = threshold_error_function(threshold, images, masks, manuals)
    error = 1;
    for i = 1:length(images)
        [true_pos, true_neg, false_pos, false_neg, ~, ~] = image_stats(imbinarize(images{i}, threshold), manuals{i}, masks{i});
        mcc = (true_pos * true_neg - false_pos*false_neg) / sqrt((true_pos + false_pos)*(true_pos + false_neg)*(true_neg + false_pos)*(true_neg + false_neg));
        error = min(error, -mcc);
    end;
end

