function [ result ] = max_conv_by_pixel( I, mask, Ks )
    % green channel seems to be the best to work with
    green = I(:,:,2);
    
    % apply 5x5 mean filter to reduce noise
    mean_filter = 1/25 * ones(5, 5);
    green = conv2(double(green), mean_filter, 'same');

    % calculate max
    A = zeros(size(green));
    for i = 1:size(Ks, 3)
        if i == 1
            A = conv2(double(green), Ks(:, :, i), 'same');
        else
            A = max(A, conv2(double(green), Ks(:, :, i), 'same'));
        end;
    end;

    % mask & scale
    A = A .* double(mask ~= 0);
    m = min(A(:));
    M = max(A(:));
    result = (A - m) / (M - m);
end

