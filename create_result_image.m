function [ result ] = create_result_image( image, mask, manual )
    [m, n] = size(image);
    result = zeros(m, n, 3);
 
     for i = 1:m
        for j = 1:n
            if mask(i, j) ~= 0
                if image(i, j) ~= 0 && manual(i, j) ~= 0
                    result(i, j, :) = [255, 255, 255];
                elseif image(i, j) ~= 0 && manual(i, j) == 0
                    result(i, j, :) = [255, 0, 0];
                elseif image(i, j) == 0 && manual(i, j) ~= 0
                    result(i, j, :) = [0, 255, 0];
                end;
            end;
        end;
     end;
end

