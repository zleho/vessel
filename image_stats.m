function [ true_pos, true_neg, false_pos, false_neg, pos, neg ] = image_stats( image, manual, mask )
    pos = 0;
    neg = 0;
    true_pos = 0;
    true_neg = 0;
    false_pos = 0;
    false_neg = 0;
    
    for i = 1:size(image, 1)
        for j = 1:size(image, 2)
            if mask(i, j) ~= 0
                if manual(i, j) ~= 0
                    pos = pos + 1;
                else
                    neg = neg + 1;
                end;
                
                if image(i, j) ~= 0 && manual(i, j) ~= 0
                    true_pos = true_pos + 1;
                elseif image(i, j) == 0 && manual(i, j) == 0
                    true_neg = true_neg + 1;
                elseif image(i, j) ~= 0 && manual(i, j) == 0
                    false_pos = false_pos + 1;
                elseif image(i, j) == 0 && manual(i, j) ~= 0
                    false_neg = false_neg + 1;
                end;
            end;
        end;
    end;
end